﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    void LoadGame(bool activatePlayerAI)
    {
        ApplicationModel.activatePlayerAI = activatePlayerAI;
        SceneManager.LoadScene("Game");
    }

    #region Events

    public void OnButtonPlayClick()
    {
        LoadGame(false);
    }

    public void OnButtonAIPlayClick()
    {
        LoadGame(true);
    }

    public void OnButtonQuitClick()
    {
        Application.Quit();
    }

    #endregion
}
