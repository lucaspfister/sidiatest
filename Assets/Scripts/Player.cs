﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    //Public variables
    public float speed;
    public float sideMoveAmount;
    public float sideMoveTime;

    [HideInInspector]
    public bool isPlaying = false;
    [HideInInspector]
    public float sideMove = 0;

    //Private variables
    Animator animator;
    bool isSideMoving = false;
    bool isPressingButton = false;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (!isPlaying)
        {
            return;
        }

        transform.localPosition += transform.forward * speed * Time.deltaTime;
        SideMove();
    }

    void SideMove()
    {
        float h = InputManager.Instance.GetHorizontalInputAxis();

        if (h == 0)
        {
            isPressingButton = false;
            return;
        }
        
        if (isSideMoving || isPressingButton)
        {
            return;
        }

        isPressingButton = true;

        if (h > 0 && sideMove <= 0)
        {
            isSideMoving = true;
            sideMove += sideMoveAmount;
            transform.DOLocalMoveX(sideMove, sideMoveTime).OnComplete(() => isSideMoving = false);
            return;
        }

        if (h < 0 && sideMove >= 0)
        {
            isSideMoving = true;
            sideMove -= sideMoveAmount;
            transform.DOLocalMoveX(sideMove, sideMoveTime).OnComplete(() => isSideMoving = false);
            return;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            Item item = other.GetComponent<Item>();
            item.Collect();

            if (item.isVictoryItem)
            {
                Stop();
                GameController.Instance.Victory();
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            Stop();
            GameController.Instance.GameOver();
        }
    }

    public void Play()
    {
        animator.SetFloat("MoveSpeed", speed);
        isPlaying = true;
    }

    public void Stop()
    {
        animator.SetFloat("MoveSpeed", 0);
        isPlaying = false;
    }
}
