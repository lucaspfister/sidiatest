﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    public bool ignoreInput;

    float h = 0;

    void Awake()
    {
        Instance = this;
    }

#if UNITY_EDITOR

    void Update()
    {
        if (!ignoreInput)
        {
            h = Input.GetAxis("Horizontal");
        }
    }

#endif

    /// <summary>
    /// Set the horizontal axis value
    /// </summary>
    /// <param name="value">horizontal axis value</param>
    /// <param name="force">force a value override?</param>
    public void SetInputH(float value, bool force = false)
    {
        if (!ignoreInput || force)
        {
            h = value;
        }
    }

    /// <summary>
    /// Return the horizontal axis value
    /// </summary>
    /// <returns></returns>
    public float GetHorizontalInputAxis()
    {
        return h;
    }

    #region Events

    public void OnButtonLeftEnter()
    {
        if (!Application.isEditor)
        {
            SetInputH(-1);
        }
    }

    public void OnButtonRightEnter()
    {
        if (!Application.isEditor)
        {
            SetInputH(1);
        }
    }

    public void OnButtonLeftExit()
    {
        if (!Application.isEditor)
        {
            SetInputH(0);
        }
    }

    public void OnButtonRightExit()
    {
        if (!Application.isEditor)
        {
            SetInputH(0);
        }
    }

    #endregion
}
