﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public float rotationSpeed;
    public GameObject item;
    public GameObject particles;
    public int points = 1;
    public bool isVictoryItem = false;

    bool isCollected = false;

    void Update()
    {
        if (!isCollected)
        {
            transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
        }
    }

    public void Collect()
    {
        GameController.Instance.AddPoints(points);
        isCollected = true;
        item.SetActive(false);
        particles.SetActive(true);
        Destroy(gameObject, 0.6f);
    }
}
