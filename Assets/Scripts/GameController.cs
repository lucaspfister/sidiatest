﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    //Public variables
    public Player player;
    public PlayerAI playerAI;
    public TextMeshPro txtCountdown;
    public TextMeshProUGUI txtPoints;
    public GameObject victoryPanel;
    public GameObject gameOverPanel;

    //Private variables
    int points = 0;

    void Awake()
    {
        Instance = this;
        playerAI.gameObject.SetActive(ApplicationModel.activatePlayerAI);
    }

    IEnumerator Start()
    {
        yield return StartCoroutine(Countdown(3));

        player.Play();
    }

    IEnumerator Countdown(int time)
    {
        txtCountdown.text = time.ToString();
        WaitForSeconds wfs = new WaitForSeconds(1f);

        while (time > 0)
        {
            yield return wfs;
            time--;
            txtCountdown.text = time.ToString();
        }
    }

    public void AddPoints(int points)
    {
        this.points += points;
        txtPoints.text = this.points.ToString();
    }

    public void GameOver()
    {
        gameOverPanel.SetActive(true);
    }

    public void Victory()
    {
        victoryPanel.SetActive(true);
    }

    #region Events

    public void OnButtonBackClick()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OnButtonPlayClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    #endregion
}
