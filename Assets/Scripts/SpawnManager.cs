﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] spawnPrefabs;

    void Start()
    {
        int randomIndex;

        foreach (Transform item in spawnPoints)
        {
            randomIndex = Random.Range(0, spawnPrefabs.Length);
            GameObject spawned = Instantiate(spawnPrefabs[randomIndex], item);
            spawned.transform.localPosition = Vector3.zero;
        }
    }
}
