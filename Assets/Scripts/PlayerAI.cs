﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAI : MonoBehaviour
{
    public Player player;
    public float raycastFrequency = 0.3f;
    public float raycastDistance = 3;

    Vector3[] raycastPoints;
    Vector3 origin;
    RaycastHit hit;
    float[] pathValues;

    WaitForSeconds wait;
    WaitForSeconds waitPlayerMovement;

    IEnumerator Start()
    {
        InputManager.Instance.ignoreInput = true;
        wait = new WaitForSeconds(raycastFrequency);
        waitPlayerMovement = new WaitForSeconds(player.sideMoveTime);

        SetRaycastPoints();
        SetPathValues();

        yield return new WaitUntil(() => player.isPlaying);

        StartCoroutine(ChoosePath());
    }

    void SetRaycastPoints()
    {
        raycastPoints = new Vector3[3];
        raycastPoints[0] = new Vector3(transform.position.x + (player.sideMoveAmount * -1), transform.position.y, 0);
        raycastPoints[1] = new Vector3(transform.position.x, transform.position.y, 0);
        raycastPoints[2] = new Vector3(transform.position.x + player.sideMoveAmount, transform.position.y, 0);
    }

    void SetPathValues()
    {
        pathValues = new float[3];
        pathValues[0] = -1;
        pathValues[1] = 0;
        pathValues[2] = 1;
    }

    IEnumerator ChoosePath()
    {
        PathOption chosenPathOption = new PathOption();
        chosenPathOption.type = PathOptionType.None;

        PathOption pathOption = new PathOption();

        for (int i = 0; i < raycastPoints.Length; i++)
        {
            pathOption.value = pathValues[i];
            origin = new Vector3(raycastPoints[i].x, raycastPoints[i].y, player.transform.position.z);

            if (Physics.Raycast(origin, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
            {
                string tag = hit.transform.tag;

                if (tag == "Item")
                {
                    pathOption.type = PathOptionType.Item;
                    pathOption.distance = hit.distance;

                    if (chosenPathOption.type == PathOptionType.Item)
                    {
                        if (pathOption.distance < chosenPathOption.distance)
                        {
                            chosenPathOption = pathOption;
                        }
                    }
                    else
                    {
                        chosenPathOption = pathOption;
                    }
                }
                else if (tag == "Obstacle")
                {
                    pathOption.type = PathOptionType.Obstacle;
                    pathOption.distance = hit.distance;

                    if (chosenPathOption.type == PathOptionType.Obstacle)
                    {
                        if (chosenPathOption.distance < pathOption.distance)
                        {
                            chosenPathOption = pathOption;
                        }
                    }
                }

                //Debug.Log(tag);
            }
            else
            {
                pathOption.type = PathOptionType.None;
                pathOption.distance = 0;

                if (chosenPathOption.type == PathOptionType.Obstacle)
                {
                    chosenPathOption = pathOption;
                }

                //Debug.Log("None");
            }

            if (i == 0)
            {
                chosenPathOption = pathOption;
            }
        }

        //Debug.Log(chosenPathOption.value);

        float value = CalculateCorrectValue(chosenPathOption.value);

        InputManager.Instance.SetInputH(value, true);

        if (value != 0)
        {
            yield return waitPlayerMovement;
            InputManager.Instance.SetInputH(0, true);
            yield return null;
        }
        else
        {
            yield return wait;
        }



        if (player.isPlaying)
        {
            StartCoroutine(ChoosePath());
        }
    }

    /// <summary>
    /// Check player position and return the correct input value
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    float CalculateCorrectValue(float value)
    {
        if (player.sideMove < 0)
        {
            return value < 0 ? 0 : 1;
        }

        if (player.sideMove > 0)
        {
            return value > 0 ? 0 : -1;
        }

        return value;
    }
}

struct PathOption
{
    public PathOptionType type { get; set; }
    public float distance { get; set; }
    public float value { get; set; }
}

enum PathOptionType
{
    None,
    Item,
    Obstacle
}
