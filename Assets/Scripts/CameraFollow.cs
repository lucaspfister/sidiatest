﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform transformToFollow;

    Vector3 lockedAxis = new Vector3(0, 1, 1);

    float relativePosZ;

    void Start()
    {
        relativePosZ = transform.localPosition.z - transformToFollow.localPosition.z;
    }

    void LateUpdate()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transformToFollow.localPosition.z + relativePosZ);
    }
}
